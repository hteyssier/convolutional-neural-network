# Convolutional Neural Network implementation 

A convolutional neural network implementation for a multi-label classification problem on the Combo MNIST dataset.

## Dataset

The Combo MNIST dataset has 30,000 labeled images to train the classifier. The dataset also comes with 15,000 test images which are used to test the accuracy of the model. Each image consists of two characters: one letter from the English alphabet (26 letters in total) and one digit from numbers 0-9 (10 digits in total). These characters can be of different size and may appear in any part of the image with different orientations. Additionally, these images may also be affected by noise. The task is to train a model that can correctly identify the characters in the image. 

## Authors

Hector Teyssier - 260840287

COMP 551 - Fall 2021 - McGill University
